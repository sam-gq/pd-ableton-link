pd-ableton-link (0.6~ds0-2) unstable; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Add patch to allow compilation with Pd>=0.52
  * Fix upstream repository
  * Apply "warp-and-sort -ast"

  [ Debian Janitor ]
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Remove constraints unnecessary since buster

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 20 Dec 2021 09:41:28 +0100

pd-ableton-link (0.6~ds0-1) unstable; urgency=medium

  * New upstream version 0.6~ds0
  * Update versionmangling and repacksuffix rules in d/watch
  * Add salsa-ci configuration
  * Bump dh-compat to 13
  * Bump standards version to 4.6.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 24 Aug 2021 16:46:57 +0200

pd-ableton-link (0.5~repack-1) unstable; urgency=medium

  * New upstream version 0.5~repack

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Added d/README to explain how to load this library
  * Declare that building this package does not require "root" powers
  * Drop obsolete d/source/local-options
  * Bump dh compat to 12
  * Bump standards version to 4.5.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 18 Feb 2020 09:27:43 +0100

pd-ableton-link (0.4~repack-1) unstable; urgency=medium

  * New upstream version 0.4~repack

  * Use upstream buildsystem
  * Repacked to exclude AblLinkSample/ folder
  * B-D on ableton-link-dev >= 3
    * Set Built-Using stanza (for ableton-link)
  * Added d/watch
  * Updated d/copyright (switching to BSD-3-clause)
    * Added 'licensecheck' target
    * Generated d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 06 Mar 2018 21:02:14 +0100

pd-ableton-link (0.2-3) unstable; urgency=medium

  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Bumped dh-compat to 11
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 29 Jan 2018 20:21:57 +0100

pd-ableton-link (0.2-2) unstable; urgency=medium

  * Use system-provided pd-lib-builder Makefile (Closes: #842822)
  * Added d/gbp.conf
  * Added d/source/local-options
  * Canoncial Vcs-Browser stanza

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 23 Nov 2016 23:00:05 +0100

pd-ableton-link (0.2-1) unstable; urgency=medium

  * Initial release (Closes: #842397)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 31 Oct 2016 16:59:26 +0100
